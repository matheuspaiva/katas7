//forEach
function newForEach(array, callbackToRunOnEachItem) {
    for (let index = 0; index < array.length; index++) {
        const currentValue = array[index]
        callbackToRunOnEachItem(currentValue, index, array)
    }
 }
//forEach

//map
function newMap(array, callback) {
    let output = []
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        let item = callback(currentValue, index, array)
        output[index] = item
        
    }
    return output;  
 }
//map

//some
function newSome(array, callback) {
    
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        const item = callback(currentValue, index, array)
        if(item === true){
            return true
        }
        
    }
    return false  
 }
//some

//find
function newFind(array, callback) {
    
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        const item = callback(currentValue, index, array)
        if(item === true){
            return currentValue
        }
        
    }
    return undefined
 }
//find


//findIndex
function newFindIndex(array, callback) {
    
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        const item = callback(currentValue, index, array)
        if(item === true){
            return index
        }
        
    }
    return -1 
 }
//findIndex


//every
function newEvery(array, callback) {
   
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        const item = callback(currentValue, index, array)
        if(item === false){
            return false
        }
        
    }
    return true;  
 }
//every


//filter
function newFilter(array, callback) {
    let output = []
    let indexOutput = 0
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        let item = callback(currentValue, index, array)
        if(item === true){
            output[indexOutput] = currentValue
            indexOutput++
        }
        
        
    }
    return output;  
 }
//filter



//concat
function newConcat() {
    let output = []
    let indexOutput = 0
    
    for (let index = 0; index < arguments.length; index++) {
        
        const currentValue = arguments[index]

        if(Array.isArray(currentValue) === true){
            
            for(let counter = 0; counter < currentValue.length; counter++){
                output[indexOutput] = currentValue[counter]
                indexOutput++
            }
        }else{
            output[indexOutput] = currentValue
            indexOutput++
        }
        
        
    }
    return output;  
 }
//concat



//includes
function newIncludes(array, searchElement, fromIndex = 0) {
    
    for (let index = fromIndex; index < array.length; index++) {
        
        const currentValue = array[index]

        if(currentValue === searchElement){
            return true
        }
        
        
    }
    return false
 }
//includes


//indexOf
function newIndexOf(array, searchElement, fromIndex = 0) {
    
    for (let index = fromIndex; index < array.length; index++) {
        
        const currentValue = array[index]

        if(currentValue === searchElement){
            return index
        }
        
        
    }
    return -1
 }
//indexOf



//join
function newJoin(array, separator) {
    let output = ''
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        if(index === array.length -1){
            output += `${currentValue}`
        }else{
            output += `${currentValue}${separator}`
        }
       
    }
    return output
 }
//join



//slice
function newSlice(array, start = 0, end = array.length) {
    let output = []
    let indexOutput = 0
    for (let index = start; index < end; index++) {
        const currentValue = array[index]
        output[indexOutput] = currentValue
        indexOutput++
       }
    return output
 }
//slice



//flat
function newFlat(array, depth = 1) {
    let output = []
    let indexOutput = 0
    let nivel = 1
    for (let index = 0; index < array.length; index++) {

        const currentValue = array[index]

        if(Array.isArray(currentValue) === true){
            console.log(currentValue)
            if(nivel <= depth){
                nivel++
                
                output = newConcat(output,newFlat(currentValue, depth - (nivel - 1)))
            }else{
                output.push(currentValue)
            }
            
        }else{
            output.push(currentValue)
            indexOutput++
        }
        
       }
    return output
 }
//flat


//flatMap
function newFlatMap(array, callback) {
    let output = []
    for (let index = 0; index < array.length; index++) {
        
        const currentValue = array[index]
        let item = callback(currentValue, index, array)
        output[index] = item
        
    }
    output = newFlat(output)
    return output;  
 }
//flatMap


//arrayOf
function newArrayOf() {
    let output = []
    for (let index = 0; index < arguments.length; index++) {
        
        const currentValue = arguments[index]
        
        output.push(currentValue)
        
    }
    
    return output;  
 }
//arrayOf